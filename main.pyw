#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import Tkinter
import os
import urllib2
import json
import ConfigParser
import vlc
from random import shuffle

track_num = 0


class AppTk(Tkinter.Tk):

    version = "0.1"

    def __init__(self, parent):
        Tkinter.Tk.__init__(self, parent)
        self.parent = parent
        self.listFrame = Tkinter.Frame(self.parent)
        self.Instance = vlc.Instance()
        self.player = self.Instance.media_list_player_new()
        self.media = self.Instance.media_list_new('')
        self.events = vlc.EventType
        self.manager = self.player.event_manager()

        self.cfg = ConfigParser.ConfigParser()
        self.cfg.read('config.ini')
        # This token you can get on a URL http://tw.fex.cc
        self.token = self.cfg.get('sc', 'client_id')
        self.playlist = self.cfg.get('sc', 'playlist')
        self.sc_url = "https://api.soundcloud.com/resolve?url=https://soundcloud.com/%s&client_id=" % self.playlist
        self.logo_size = 32, 32
        self.create_menubar()
        self.initialize()

    def create_menubar(self):
        menubar = Tkinter.Menu(self)
        filemenu = Tkinter.Menu(self, menubar, tearoff=0)

        filemenu.add_command(label="Exit", command=self.destroy_ui, accelerator="Ctrl+D")
        menubar.add_cascade(label="File", menu=filemenu)

        helpmenu = Tkinter.Menu(menubar, tearoff=0)
        helpmenu.add_command(label="About...", command=self.create_window_about)
        menubar.add_cascade(label="Help", menu=helpmenu)

        self.config(menu=menubar)
        self.bind_all("<Control-d>", self.destroy_ui)

    def get_sc(self):
        try:
            response = urllib2.urlopen('%s%s' % (self.sc_url, self.token))
            data = json.load(response)
            return data
        except urllib2.HTTPError:
            return False

    def initialize(self):
        self.listFrame.pack(side="left", expand=True, fill="both", padx=10, pady=10)
        self.fetch_data()

    def button_play_stream(self, sc_data):
        global track_num
        play_uri = ['%s/stream?client_id=%s' % (item['uri'], self.token) for item in sc_data]
        media = self.Instance.media_list_new(play_uri)
        self.player.set_media_list(media)
        self.manager = self.player.event_manager()
        self.manager.event_attach(vlc.EventType.MediaListPlayerNextItemSet, self.state_callback, sc_data)

        self.player.play()

    def state_callback(self, event, sc_title):
        print event
        global track_num
        self.render_gui(sc_title, track_num)
        track_num += 1

    def button_stop_stream(self):
        self.player.stop()

    def create_window_about(self):
        t = Tkinter.Toplevel(self)
        t.wm_title("About...")
        l = Tkinter.Label(t, text="This is soundcloud player v%s" % self.version)
        l.pack(side="top", fill="both", expand=True, padx=100, pady=100)

    def restart_ui(self, event=""):
        print "Restarting .... %s" % event
        self.destroy()
        main()

    def destroy_ui(self, event=""):
        print "Quit .... %s" % event
        self.destroy()

    def fetch_data(self):
        streamers = self.get_sc()
        play_list = []

        if bool(streamers):
            for j, i in enumerate(streamers['tracks']):
                play_list.append(
                    dict(uri=i['uri'], img=i['artwork_url'], title=i['title'], usr=i['user']['username']))
            shuffle(play_list)

        else:
            Tkinter.Label(self, text="Wrong token in config.ini OR twitch servers unavailable",
                          justify=Tkinter.LEFT).grid()

        self.button_play_stream(play_list)

    def render_gui(self, data, pos):
        sc_col = {
            'title': data[pos]['title'],
            'usr': data[pos]['usr'] + " -",
        }

        for k, col in enumerate(sc_col.values()):
            try:
                col = unicode(col)
                Tkinter.Label(self.listFrame, text=col[:164].encode('utf-8'),
                              justify=Tkinter.LEFT).grid(row=0, column=k + 1, sticky=Tkinter.W)
            except Tkinter.TclError:
                Tkinter.Label(self.listFrame, text=":)", justify=Tkinter.LEFT).grid(row=0, column=k + 1,
                                                                                    sticky=Tkinter.W)


def main():
    app = AppTk(None)
    app.title('Soundcloud View')
    if "nt" == os.name:
        app.wm_iconbitmap(bitmap="icon.ico")
    else:
        app.wm_iconbitmap(bitmap="@icon.xbm")
    app.mainloop()

if __name__ == "__main__":
    main()
