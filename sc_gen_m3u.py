#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    This script is generating a *.m3u playlist from soundcloud


import urllib2
import json
import ConfigParser

version = "0.1"

cfg = ConfigParser.ConfigParser()
cfg.read('config.ini')
# This token you can get on a URL http://tw.fex.cc
token = cfg.get('sc', 'client_id')
playlist = cfg.get('sc', 'playlist')
sc_url = "https://api.soundcloud.com/resolve?url=https://soundcloud.com/%s&client_id=" % playlist


def main():
    response = urllib2.urlopen('%s%s' % (sc_url, token))
    data = json.load(response)

    fp = open("sc.m3u", "w")
    fp.write("#EXTM3U\n")
    if data:
        for j, i in enumerate(data['tracks']):
            fp.write("#EXTINF:%s,%s - %s\n" % (int(i['duration']/1000), i['user']['username'].encode('utf8'), i['title'].encode('utf8')))
            fp.write("%s/stream?client_id=%s\n" % (i['uri'], token))
    fp.close()

if __name__ == "__main__":
    main()
