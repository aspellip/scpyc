#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import Tkinter
import os
import urllib2
import urllib
import cStringIO
import json
import subprocess
import ConfigParser
from functools import partial
from PIL import Image, ImageTk
import vlc


class AppTk(Tkinter.Tk):

    version = "0.1"

    def __init__(self, parent):
        Tkinter.Tk.__init__(self, parent)
        self.parent = parent

        self.Instance = vlc.Instance()
        self.player = self.Instance.media_player_new()

        self.cfg = ConfigParser.ConfigParser()
        self.cfg.read('config.ini')
        # This token you can get on a URL http://tw.fex.cc
        self.token = self.cfg.get('sc', 'client_id')
        self.sc_url = "https://api.soundcloud.com/resolve?url=https://soundcloud.com/aspel-lip/sets/car-mus&client_id="
        self.logo_size = 32, 32
        self.create_menubar()
        self.initialize()

    def create_menubar(self):
        menubar = Tkinter.Menu(self)
        filemenu = Tkinter.Menu(self, menubar, tearoff=0)
        filemenu.add_command(label="Reload", command=self.restart_ui, accelerator="Ctrl+R")

        filemenu.add_separator()

        filemenu.add_command(label="Exit", command=self.destroy_ui, accelerator="Ctrl+D")
        menubar.add_cascade(label="File", menu=filemenu)

        helpmenu = Tkinter.Menu(menubar, tearoff=0)
        helpmenu.add_command(label="About...", command=self.create_window_about)
        menubar.add_cascade(label="Help", menu=helpmenu)

        self.config(menu=menubar)
        self.bind_all("<Control-r>", self.restart_ui)
        self.bind_all("<Control-d>", self.destroy_ui)

    def get_sc(self):
        try:
            response = urllib2.urlopen('%s%s' % (self.sc_url, self.token))
            data = json.load(response)
            return data
        except urllib2.HTTPError:
            return False

    def initialize(self):
        self.frameOne = Tkinter.Frame(self.parent)
        self.frameOne.pack(side="left", expand = True, fill="both",padx = 10, pady = 10)

        self.canvas = Tkinter.Canvas(self.frameOne)
        self.canvas.pack(side="left", fill="both", expand=True)

        self.listFrame = Tkinter.Frame(self.canvas)

        self.canvas_frame = self.canvas.create_window((0, 0), window=self.listFrame, anchor='nw')

        self.scrollb = Tkinter.Scrollbar(self.canvas, orient="vertical", command=self.canvas.yview)
        self.scrollb.pack(side="right", fill="y")

        self.canvas.config(yscrollcommand = self.scrollb.set)
        self.fetch_data()
        self.listFrame.bind("<Configure>", self.OnFrameConfigure)

    def OnFrameConfigure(self, event):
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))

    def button_play_stream(self, sc_name):
        media = self.Instance.media_new(sc_name)
        self.player.set_media(media)
        self.player.play()

    def button_stop_stream(self, sc_name):
        self.player.stop()

    def create_window_about(self):
        t = Tkinter.Toplevel(self)
        t.wm_title("About...")
        l = Tkinter.Label(t, text="This is soundcloud player v%s" % self.version)
        l.pack(side="top", fill="both", expand=True, padx=100, pady=100)

    def restart_ui(self, event=""):
        print "Restarting .... %s" % event
        self.destroy()
        main()

    def destroy_ui(self, event=""):
        print "Quit .... %s" % event
        self.destroy()

    def fetch_data(self):
        streamers = self.get_sc()
        if bool(streamers):
            for j, i in enumerate(streamers['tracks']):
                # Get logo images of streamers and resize them
                print i['artwork_url']
                try:
                    img_data = cStringIO.StringIO(urllib.urlopen(i['artwork_url']).read())
                    image = Image.open(img_data)
                    image.thumbnail(self.logo_size, Image.ANTIALIAS)
                    photo = ImageTk.PhotoImage(image)
                except:
                    image = Image.open("unknown.jpg")
                    image.thumbnail(self.logo_size, Image.ANTIALIAS)
                    photo = ImageTk.PhotoImage(image)

                sc_col = {
                    'title': i['title'],
                }
                # Logo is a first in a row.
                label_img = Tkinter.Label(self.listFrame, image=photo, justify=Tkinter.LEFT)
                label_img.image = photo
                label_img.grid(row=j, column=0)

                for k, col in enumerate(sc_col.values()):
                    try:
                        col = unicode(col)
                        Tkinter.Label(self.listFrame, text=col[:164].encode('utf-8'), justify=Tkinter.LEFT).grid(row=j,
                                                                                                                 column=k + 1,
                                                                                                                 sticky=Tkinter.W)
                    except Tkinter.TclError:
                        Tkinter.Label(self.listFrame, text=":)", justify=Tkinter.LEFT).grid(row=j, column=k + 1,
                                                                                            sticky=Tkinter.W)

                # Button is a last in a row
                Tkinter.Button(self.listFrame, text="Play", justify=Tkinter.LEFT,
                               command=partial(self.button_play_stream,
                                               '%s/stream?client_id=%s' % (i['uri'], self.token))).grid(row=j,
                                                                                                        column=len(
                                                                                                            sc_col) + 2,
                                                                                                        sticky=Tkinter.W)
                Tkinter.Button(self.listFrame, text="Stop", justify=Tkinter.LEFT,
                               command=partial(self.button_stop_stream,
                                               '%s/stream?client_id=%s' % (i['uri'], self.token))).grid(row=j,
                                                                                                        column=len(
                                                                                                            sc_col) + 3,
                                                                                                        sticky=Tkinter.W)
        else:
            Tkinter.Label(self, text="Wrong token in config.ini OR twitch servers unavailable",
                          justify=Tkinter.LEFT).grid()


def main():
    app = AppTk(None)
    app.title('Soundcloud View')
    if "nt" == os.name:
        app.wm_iconbitmap(bitmap="icon.ico")
    else:
        app.wm_iconbitmap(bitmap="@icon.xbm")
    app.mainloop()

if __name__ == "__main__":
    main()
